module Main exposing (..)

import Bootstrap.CDN as CDN
import Bootstrap.Grid as Grid
import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (keyCode, onClick, onFocus, onInput, preventDefaultOn)
import Http
import Json.Decode as D
import Json.Encode as E
import List as L
import Maybe as M
import Menu as Menu
import String as S



-- CONFIG


howManyToShow : Int
howManyToShow =
    5


howManyToFetch : Int
howManyToFetch =
    10


conceptsEndpoint : String
conceptsEndpoint =
    "https://taxonomy.api.jobtechdev.se"


autocompleteEndpoint : String
autocompleteEndpoint =
    "https://taxonomy.api.jobtechdev.se/test/v1/taxonomy/suggesters/autocomplete"


trainingDataEndpoint : String
trainingDataEndpoint =
    "http://localhost:8080/feedback"



-- MAIN


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type alias Concept =
    { label : String
    }


type alias Model =
    { text : String
    , lastWord : String
    , autoState : Menu.State
    , showMenu : Bool
    , concepts : List ( String, Bool )
    , autocompletes : List Concept
    , status : Maybe String
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { text = ""
      , lastWord = ""
      , autoState = Menu.empty
      , showMenu = False
      , concepts = []
      , autocompletes = []
      , status = Nothing
      }
    , Cmd.none
    )



-- UPDATE


type Msg
    = Change String
    | GetConcepts
    | GotConcepts (Result Http.Error (List String))
    | GotAutocompletes (Result Http.Error (List String))
    | SetAutoState Menu.Msg
    | SelectConcept String
    | Reset
    | OnFocus
    | NoOp
    | SetFeedback Int
    | SendFeedback
    | Feedback (Result Http.Error ())


fetchConcepts : Cmd Msg
fetchConcepts =
    Http.request
        { body = Http.emptyBody
        , expect = Http.expectJson GotConcepts (D.list (D.field "taxonomy/preferred-label" D.string))
        , headers = [ Http.header "api-key" "111" ]
        , method = "get"
        , timeout = Nothing
        , tracker = Nothing
        , url = conceptsEndpoint ++ "/test/v1/taxonomy/main/concepts?type=wage-type"
        }


encodeList : List ( String, Bool ) -> E.Value
encodeList concepts =
    E.list (\( c, b ) -> E.object [ ( "concept", E.string c ), ( "correct", E.bool b ) ]) concepts


encode : String -> List ( String, Bool ) -> E.Value
encode context concepts =
    E.object
        [ ( "context", E.string context )
        , ( "feedback", encodeList concepts )
        ]


submitFeedback : String -> List ( String, Bool ) -> Cmd Msg
submitFeedback context concepts =
    Http.post
        { url = trainingDataEndpoint
        , body = Http.jsonBody (encode context concepts)
        , expect = Http.expectWhatever Feedback
        }


getAutocompleteWords : String -> Cmd Msg
getAutocompleteWords word =
    Http.request
        { body = Http.emptyBody
        , expect = Http.expectJson GotAutocompletes (D.list (D.field "taxonomy/preferred-label" D.string))
        , headers = [ Http.header "api-key" "111" ]
        , method = "get"
        , timeout = Nothing
        , tracker = Nothing
        , url = autocompleteEndpoint ++ "?limit=" ++ S.fromInt howManyToFetch ++ "&query-string=" ++ word
        }


containsIgnoreCase : String -> String -> Bool
containsIgnoreCase s str =
    S.contains (S.toLower s) (S.toLower str)


filterConcepts : Model -> List Concept
filterConcepts { lastWord, autocompletes } =
    let
        lowerWord =
            String.toLower lastWord
    in
    List.filter (String.contains lowerWord << String.toLower << .label) autocompletes


updateConfig : Menu.UpdateConfig Msg Concept
updateConfig =
    Menu.updateConfig
        { toId = .label
        , onKeyDown =
            \code maybeId ->
                if code == 13 then
                    Maybe.map SelectConcept maybeId

                else
                    Nothing
        , onTooLow = Nothing
        , onTooHigh = Nothing
        , onMouseEnter = \_ -> Nothing
        , onMouseLeave = \_ -> Nothing
        , onMouseClick = \id -> Just <| SelectConcept id
        , separateSelections = False
        }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Change txt ->
            let
                lastWord =
                    M.withDefault "" <| L.head <| L.reverse <| S.split " " txt

                longEnough =
                    S.length lastWord > 2
            in
            ( { model
                | text = txt
                , lastWord = lastWord
                , showMenu = longEnough
              }
            , if longEnough then
                getAutocompleteWords lastWord

              else
                Cmd.none
            )

        GetConcepts ->
            ( model
            , fetchConcepts
            )

        GotConcepts result ->
            case result of
                Ok concepts ->
                    ( { model
                        | concepts = L.map (\c -> ( c, True )) concepts
                        , status = Just "Got concepts"
                      }
                    , Cmd.none
                    )

                Err _ ->
                    ( { model | status = Just "Failed to get concepts" }
                    , Cmd.none
                    )

        GotAutocompletes result ->
            case result of
                Ok words ->
                    ( { model
                        | autocompletes = L.map (\w -> Concept w) words
                        , status = Nothing
                      }
                    , Cmd.none
                    )

                Err _ ->
                    ( { model | status = Just "Failed to get autocompletes" }
                    , Cmd.none
                    )

        SetAutoState autoMsg ->
            let
                ( newState, maybeMsg ) =
                    Menu.update updateConfig autoMsg howManyToShow model.autoState (filterConcepts model)

                newModel =
                    { model | autoState = newState }
            in
            case maybeMsg of
                Nothing ->
                    ( newModel
                    , Cmd.none
                    )

                Just updateMsg ->
                    update updateMsg newModel

        Reset ->
            ( { model
                | autoState =
                    Menu.resetToFirstItem updateConfig (filterConcepts model) howManyToShow model.autoState
              }
            , Cmd.none
            )

        SelectConcept id ->
            ( { model
                | lastWord = ""
                , text = S.dropRight (S.length model.lastWord) model.text ++ id
                , autoState = Menu.empty
                , showMenu = False
              }
            , Cmd.none
            )

        OnFocus ->
            ( model, Cmd.none )

        NoOp ->
            ( model, Cmd.none )

        SetFeedback idx ->
            ( { model
                | concepts =
                    L.map2
                        (\( c, b ) i ->
                            if i == idx then
                                ( c, not b )

                            else
                                ( c, b )
                        )
                        model.concepts
                        (L.range 1 (L.length model.concepts))
              }
            , Cmd.none
            )

        SendFeedback ->
            ( model
            , submitFeedback model.text model.concepts
            )

        Feedback result ->
            case result of
                Ok _ ->
                    ( { model | status = Just "Feedback submitted!" }
                    , Cmd.none
                    )

                Err _ ->
                    ( { model | status = Just "Failed to submit feedback" }
                    , Cmd.none
                    )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.map SetAutoState Menu.subscription



-- VIEW


displayFeedbackButtons : Model -> Html Msg
displayFeedbackButtons model =
    let
        range : List Int
        range =
            L.range 1 (L.length model.concepts)

        btn : ( String, Bool ) -> Int -> Html Msg
        btn ( c, b ) i =
            let
                txt =
                    if b then
                        "btn btn-success"

                    else
                        "btn"
            in
            span []
                [ button
                    [ onClick (SetFeedback i)
                    , class txt
                    ]
                    [ text c ]
                ]
    in
    div []
        (L.map2 btn model.concepts range)


displayStatus : Maybe String -> Html Msg
displayStatus mStr =
    case mStr of
        Nothing ->
            div [] []

        Just str ->
            div [] [ label [] [ text str ] ]


view : Model -> Html Msg
view model =
    let
        upDownDecoderHelper : Int -> D.Decoder ( Msg, Bool )
        upDownDecoderHelper code =
            if code == 38 || code == 40 then
                D.succeed ( NoOp, True )

            else
                D.fail "not handling that key"

        upDownDecoder : D.Decoder ( Msg, Bool )
        upDownDecoder =
            keyCode |> D.andThen upDownDecoderHelper
    in
    Grid.container []
        [ CDN.stylesheet -- creates an inline style node with the Bootstrap CSS
        , Grid.row []
            [ Grid.col []
                [ div []
                    [ textarea
                        [ rows 5
                        , placeholder "Skriv kompetens ord här"
                        , value model.text
                        , onInput Change
                        , onFocus OnFocus
                        , preventDefaultOn "keydown" upDownDecoder
                        , class "autocomplete-input"
                        ]
                        []
                    , if model.showMenu then
                        viewMenu model

                      else
                        Html.div [] []
                    ]
                , div [] [ button [ onClick GetConcepts ] [ text "Get concepts" ] ]
                , displayFeedbackButtons model
                , button [ onClick SendFeedback ] [ text "Send Feedback" ]
                , displayStatus model.status
                ]
            ]
        ]


viewMenu : Model -> Html.Html Msg
viewMenu model =
    Html.div [ class "autocomplete-menu" ]
        [ Html.map SetAutoState <|
            Menu.view
                viewConfig
                howManyToShow
                model.autoState
                (filterConcepts model)
        ]


viewConfig : Menu.ViewConfig Concept
viewConfig =
    let
        customizedLi keySelected mouseSelected concept =
            { attributes =
                [ classList
                    [ ( "autocomplete-item", True )
                    , ( "key-selected", keySelected )
                    , ( "mouse-selected", mouseSelected )
                    ]
                ]
            , children = [ Html.text concept.label ]
            }
    in
    Menu.viewConfig
        { toId = .label
        , ul = [ class "autocomplete-list" ]
        , li = customizedLi
        }
